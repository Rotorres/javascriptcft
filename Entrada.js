function mostrarDatosAtencion() {
  var formulario = document.getElementById("formulario");
  formulario.style.display = "block";
}

function guardarMascota() {
  const datosFormulario = {
    nombreMascota: document.getElementById("nombre_mascota").value,
    tipoMascota: document.getElementById("tipo_mascota").value,
    nombreDueno: document.getElementById("nombre_dueno").value,
    numContacto: document.getElementById("num_contacto").value
  };

  const mascotasGuardadas = JSON.parse(localStorage.getItem("mascotas")) || [];
  mascotasGuardadas.push(datosFormulario);
  localStorage.setItem("mascotas", JSON.stringify(mascotasGuardadas));

  mostrarEnTabla(datosFormulario);

  alert("Mascota guardada exitosamente");
  document.getElementById("mascotaForm").reset();
}

function mostrarEnTabla(datosMascota) {
  const tabla = document.getElementById("tablaMascotas");
  const fila = document.createElement("tr");

  for (const [nombreCampo, valorCampo] of Object.entries(datosMascota)) {
    const celda = document.createElement("td");
    celda.textContent = valorCampo;
    fila.appendChild(celda);
  }

  tabla.querySelector("tbody").appendChild(fila);
}


document.addEventListener("DOMContentLoaded", function () {
  const mascotasGuardadas = JSON.parse(localStorage.getItem("mascotas")) || [];
  for (const mascota of mascotasGuardadas) {
    mostrarEnTabla(mascota);
  }
});


function mostrarEnTabla(datosMascota) {
  const tabla = document.getElementById("tablaMascotas");
  const fila = document.createElement("tr");

  for (const [nombreCampo, valorCampo] of Object.entries(datosMascota)) {
    const celda = document.createElement("td");
    celda.textContent = valorCampo;
    fila.appendChild(celda);
  }

  const accionesCelda = document.createElement("td");

  // Botón  editar
  const botonEditar = document.createElement("button");
  botonEditar.textContent = "Editar";
  botonEditar.className = "btn btn-warning btn-sm mr-1";
  botonEditar.onclick = function() {
    editarMascota(datosMascota);
  };
  accionesCelda.appendChild(botonEditar);

  // Botón  borrar
  const botonBorrar = document.createElement("button");
  botonBorrar.textContent = "Borrar";
  botonBorrar.className = "btn btn-danger btn-sm";
  botonBorrar.onclick = function() {
    borrarMascota(datosMascota);
  };
  accionesCelda.appendChild(botonBorrar);

  fila.appendChild(accionesCelda);

  tabla.querySelector("tbody").appendChild(fila);
}

function editarMascota(datosMascota) {
 
}

function borrarMascota(datosMascota) {
  const mascotasGuardadas = JSON.parse(localStorage.getItem("mascotas")) || [];
  const mascotasActualizadas = mascotasGuardadas.filter(mascota => mascota !== datosMascota);
  localStorage.setItem("mascotas", JSON.stringify(mascotasActualizadas));

  // quitar tabla
  const tabla = document.getElementById("tablaMascotas");
  const filas = tabla.querySelectorAll("tbody tr");
  filas.forEach(fila => {
    if (fila.textContent.includes(datosMascota.nombreMascota)) {
      fila.remove();
    }
  });

  alert("Mascota borrada exitosamente");
}


document.addEventListener("DOMContentLoaded", function () {
  const mascotasGuardadas = JSON.parse(localStorage.getItem("mascotas")) || [];
  for (const mascota of mascotasGuardadas) {
    mostrarEnTabla(mascota);
  }
});